db.createCollection("albums");

db.albums.insertMany([
    {
    title: "Taylor Swift",
    artist: "Taylor Swift",
    url: "https://www.amazon.com/Taylor-Swift/dp/B0014I4KH6",
    image: "https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
    thumbnail_image: "https://i.imgur.com/K3KJ3w4h.jpg",
    songs:[
      {
        artist: "Taylor Swift",
        title: "Picture to Born",
        albumArtUrl: "https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
        audioUrl: "https://albumapp-api.herokuapp.com/Picture_To_Born.mp3"
      },
      {
        artist: "Taylor Swift",
        title: "Love Story",
        albumArtUrl:"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
        audioUrl: "https://albumapp-api.herokuapp.com/Love_Story.mp3"
      }
    ]
  },
  {
    title: "Fearless",
    artist: "Taylor Swift",
    url: "https://www.amazon.com/Fearless-Enhanced-Taylor-Swift/dp/B001EYGOEM",
    image: "https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg",
    thumbnail_image: "https://i.imgur.com/K3KJ3w4h.jpg",
    songs:[
      {
        artist: "Taylor Swift",
        title: "White Horse",
        albumArtUrl:"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
        audioUrl: "https://albumapp-api.herokuapp.com/White_Horse.mp3"
      },
      {
        artist: "Taylor Swift",
        title: "Tell Me Why",
        albumArtUrl:"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
        audioUrl: "https://albumapp-api.herokuapp.com/Tell_Me_Why.mp3"
      }
    ]
  },
  {
    title: "Speak Now",
    artist: "Taylor Swift",
    url: "https://www.amazon.com/Speak-Now-Taylor-Swift/dp/B003WTE886",
    image: "https://images-na.ssl-images-amazon.com/images/I/51vlGuX7%2BFL.jpg",
    thumbnail_image: "https://i.imgur.com/K3KJ3w4h.jpg",
    songs:[
      {
        artist: "Taylor Swift",
        title: "Superman",
        albumArtUrl:"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
        audioUrl: "https://albumapp-api.herokuapp.com/Superman.mp3"
      },
      {
        artist: "Taylor Swift",
        title: "Dear John",
        albumArtUrl:"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
        audioUrl: "https://albumapp-api.herokuapp.com/Dear_John.mp3"
      }
    ]
  },
  {
    title: "Red",
    artist: "Taylor Swift",
    url: "https://www.amazon.com/Red-Taylor-Swift/dp/B008XNZMOU",
    image: "https://images-na.ssl-images-amazon.com/images/I/41j7-7yboXL.jpg",
    thumbnail_image: "https://i.imgur.com/K3KJ3w4h.jpg",
    songs:[
      {
        artist: "Taylor Swift",
        title: "State of Grace",
        albumArtUrl:"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
        audioUrl: "https://albumapp-api.herokuapp.com/State_Of_Grace.mp3"
      },
      {
        artist: "Taylor Swift",
        title: "Begin Again",
        albumArtUrl:"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
        audioUrl: "https://albumapp-api.herokuapp.com/Begin_Again.mp3"
      }
    ]
  },
  {
    title: "1989",
    artist: "Taylor Swift",
    url: "https://www.amazon.com/1989-Taylor-Swift/dp/B00MRHANNI",
    image: "https://images-na.ssl-images-amazon.com/images/I/717DWgRftmL._SX522_.jpg",
    thumbnail_image: "https://i.imgur.com/K3KJ3w4h.jpg",
    songs:[
      {
        artist: "Taylor Swift",
        title: "Blank Space",
        albumArtUrl:"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
        audioUrl: "https://albumapp-api.herokuapp.com/Blank_Space.mp3"
      },
      {
        artist: "Taylor Swift",
        title: "Bad Blood",
        albumArtUrl:"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
        audioUrl: "https://albumapp-api.herokuapp.com/Bad_Blood.mp3"
      }
    ]
  }
]);