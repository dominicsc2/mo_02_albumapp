const mongoose = require('mongoose');

mongoose.connect('mongodb://admin:admin@cluster0-shard-00-00-rbuhg.mongodb.net:27017'+
',cluster0-shard-00-01-rbuhg.mongodb.net:27017,'
+'cluster0-shard-00-02-rbuhg.mongodb.net:27017/db_albums?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true');

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Mongoose connected');
});

var SongSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    artist: String,
    title: String,
    albumArtUrl: String,
    audioUrl: String,
});
/*
Agregar num de likes, género
*/
var AlbumSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: String,
    artist: String,
    url: String,
    image: String,
    thumbnail_image: String,
    songs: [SongSchema],
    likes: Number,
    genre: String,
});

module.exports = mongoose.model('album',AlbumSchema);