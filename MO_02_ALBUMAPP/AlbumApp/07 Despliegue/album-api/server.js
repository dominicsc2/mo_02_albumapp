//Dependencies
const http = require('http');
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');

//Init
const PORT = process.env.PORT || 3000;
const app = express();

//Middleware
app.use((req,res,next) => {
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(morgan('dev'));
app.use(express.static(path.join(__dirname,'files')));

//Routes
const albumRouter = require('./routes/album-router');
app.use('/albums',albumRouter);
//Error handling
app.use((err,req,res,next) => {
    if(err.status){
        res.status(err.status).send(err.message);
    }else{
        res.status(500).send();
    }
});
//Server
const server = http.createServer(app);
server.listen(PORT, () => {
    console.log(`Server running at port ${PORT}`);
});