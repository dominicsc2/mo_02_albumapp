const express = require('express');
const albumRouter = express.Router();
const mongoose = require('mongoose');
const Album = require('../models/album');

module.exports = albumRouter;

// /albums/?genre&offset&limit
albumRouter.get('/', (req,res) => {
    const offset = 0 || parseInt(req.query.offset);
    const limit = 0 || parseInt(req.query.limit); // A limit value of 0 is equivalent to setting no limit.
    const genre = req.query.genre;
    console.log(genre);
    if(genre){
        const regex = new RegExp(genre,'i');
        console.log(regex);
        Album.find({genre: regex}).sort({likes: -1}).skip(offset).limit(limit).exec()
            .then(docs => {
                res.status(200).send(docs);
            }).catch(err =>{
                res.status(500).send({error: err});
            });
    }else{
        Album.find().sort({likes: -1}).skip(offset).limit(limit).exec()
        .then(docs => {
            res.status(200).send(docs);
        }).catch(err =>{
            res.status(500).send({error: err});
        });
    }
});

albumRouter.get('/genres', (req,res) => {
    Album.find().distinct('genre').exec()
        .then(docs => {
            const genres = [];
            docs.forEach(doc => {
                genres.push(doc);
            });
            res.status(200).send(genres);
        })
        .catch(err => {
            res.status(500).send({error: err});
        });
});

albumRouter.post('/', (req,res) => {
    const {title,artist,url,image,thumbnail_image,songs,genre,likes} = req.body;
    const album = new Album({
        _id: new mongoose.Types.ObjectId(),
        title,
        artist,
        url,
        thumbnail_image,
        image,
        songs,
        genre,
        likes
    });
    album.save()
    .then(result => {
        res.status(201).send({message: "Handling post request /albums", createdAlbum: result});
    })
    .catch(err => {
        console.log(err);
        res.status(500).send({error: err});
    });
});
// albums/id
albumRouter.put('/:id', (req,res) => {
    const {likes} = req.body;
    const id = req.params.id;
    console.log(req.body);
    Album.findByIdAndUpdate(id,{$set: { likes }}, (err,doc) => {
        if(err){
            res.status(500).send({error: err});
        }
        res.status(200).send(`Updated succesful: ${doc.artist} - ${doc.title}`);
    });
});