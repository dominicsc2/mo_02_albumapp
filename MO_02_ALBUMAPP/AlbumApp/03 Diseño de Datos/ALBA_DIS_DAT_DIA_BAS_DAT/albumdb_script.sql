
-- ****************** SqlDBM: Microsoft SQL Server ******************
-- ******************************************************************

-- ************************************** [usuarios]

create database Albumdb
GO
USE Albumdb

CREATE TABLE [usuarios]
(
 [ID]       INT NOT NULL ,
 [nombre]   VARCHAR(50) NOT NULL ,
 [edad]     INT NOT NULL ,
 [correo]   VARCHAR(50) NOT NULL ,
 [nickname] VARCHAR(50) NOT NULL ,
 [password] VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_usuarios] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO





-- ************************************** [generos]

CREATE TABLE [generos]
(
 [ID]     INT NOT NULL ,
 [nombre] VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_generos] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO





-- ************************************** [productoras]

CREATE TABLE [productoras]
(
 [nombre] VARCHAR(50) NOT NULL ,
 [ID]     INT NOT NULL ,

 CONSTRAINT [PK_productoras] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO





-- ************************************** [artistas]

CREATE TABLE [artistas]
(
 [ID]     INT NOT NULL ,
 [nombre] VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_Artistas] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO





-- ************************************** [playlists]

CREATE TABLE [playlists]
(
 [ID]      INT NOT NULL ,
 [nombre]  VARCHAR(50) NOT NULL ,
 [ID_user] INT NOT NULL ,

 CONSTRAINT [PK_playlists] PRIMARY KEY CLUSTERED ([ID] ASC),
 CONSTRAINT [FK_95] FOREIGN KEY ([ID_user])
  REFERENCES [usuarios]([ID])
);
GO


--SKIP Index: [fkIdx_95]




-- ************************************** [compras]

CREATE TABLE [compras]
(
 [ID]      INT NOT NULL ,
 [fecha]   DATETIME NOT NULL ,
 [Total]   FLOAT NOT NULL ,
 [ID_user] INT NOT NULL ,

 CONSTRAINT [PK_compras] PRIMARY KEY CLUSTERED ([ID] ASC),
 CONSTRAINT [FK_59] FOREIGN KEY ([ID_user])
  REFERENCES [usuarios]([ID])
);
GO


--SKIP Index: [fkIdx_59]




-- ************************************** [albumes]

CREATE TABLE [albumes]
(
 [ID]                INT NOT NULL ,
 [nombre]            VARCHAR(50) NOT NULL ,
 [fecha_lanzamiento] DATETIME NOT NULL ,
 [ID_productora]     INT NOT NULL ,
 [precio]            Float NOT NULL ,
 [duracion]          FLOAT NOT NULL ,

 CONSTRAINT [PK_albumes] PRIMARY KEY CLUSTERED ([ID] ASC),
 CONSTRAINT [FK_20] FOREIGN KEY ([ID_productora])
  REFERENCES [productoras]([ID])
);
GO


--SKIP Index: [fkIdx_20]




-- ************************************** [listas_favoritos]

CREATE TABLE [listas_favoritos]
(
 [ID_user]  INT NOT NULL ,
 [ID_album] INT NOT NULL ,

 CONSTRAINT [PK_listas_favoritos] PRIMARY KEY CLUSTERED ([ID_user] ASC, [ID_album] ASC),
 CONSTRAINT [FK_84] FOREIGN KEY ([ID_user])
  REFERENCES [usuarios]([ID]),
 CONSTRAINT [FK_88] FOREIGN KEY ([ID_album])
  REFERENCES [albumes]([ID])
);
GO


--SKIP Index: [fkIdx_84]

--SKIP Index: [fkIdx_88]




-- ************************************** [detalle_compras]

CREATE TABLE [detalle_compras]
(
 [ID_album]  INT NOT NULL ,
 [ID_compra] INT NOT NULL ,
 [subtotal]  FLOAT NOT NULL ,

 CONSTRAINT [PK_detalle_compras] PRIMARY KEY CLUSTERED ([ID_album] ASC, [ID_compra] ASC),
 CONSTRAINT [FK_77] FOREIGN KEY ([ID_album])
  REFERENCES [albumes]([ID]),
 CONSTRAINT [FK_80] FOREIGN KEY ([ID_compra])
  REFERENCES [compras]([ID])
);
GO


--SKIP Index: [fkIdx_77]

--SKIP Index: [fkIdx_80]




-- ************************************** [canciones]

CREATE TABLE [canciones]
(
 [ID]        INT NOT NULL ,
 [nombre]    VARCHAR(50) NOT NULL ,
 [duracion]  FLOAT NOT NULL ,
 [ID_genero] INT NOT NULL ,
 [ID_album]  INT NOT NULL ,

 CONSTRAINT [PK_cancion] PRIMARY KEY CLUSTERED ([ID] ASC),
 CONSTRAINT [FK_28] FOREIGN KEY ([ID_genero])
  REFERENCES [generos]([ID]),
 CONSTRAINT [FK_31] FOREIGN KEY ([ID_album])
  REFERENCES [albumes]([ID])
);
GO


--SKIP Index: [fkIdx_28]

--SKIP Index: [fkIdx_31]




-- ************************************** [detalle_playlist]

CREATE TABLE [detalle_playlist]
(
 [ID_cancion]  INT NOT NULL ,
 [ID_playlist] INT NOT NULL 

 CONSTRAINT [PK_detalle_playlist] PRIMARY KEY CLUSTERED ([ID_cancion] ASC, [ID_playlist] ASC),
 CONSTRAINT [FK_99] FOREIGN KEY ([ID_cancion])
  REFERENCES [canciones]([ID]),
 CONSTRAINT [FK_103] FOREIGN KEY ([ID_playlist])
  REFERENCES [playlists]([ID])
);
GO


--SKIP Index: [fkIdx_99]

--SKIP Index: [fkIdx_103]




-- ************************************** [detalle_artistas]

CREATE TABLE [detalle_artistas]
(
 [ID_artista] INT NOT NULL ,
 [ID_cancion] INT NOT NULL ,

 CONSTRAINT [PK_detalle_artistas] PRIMARY KEY CLUSTERED ([ID_artista] ASC, [ID_cancion] ASC),
 CONSTRAINT [FK_40] FOREIGN KEY ([ID_artista])
  REFERENCES [canciones]([ID]),
 CONSTRAINT [FK_43] FOREIGN KEY ([ID_cancion])
  REFERENCES [artistas]([ID])
);
GO


--SKIP Index: [fkIdx_40]

--SKIP Index: [fkIdx_43]




